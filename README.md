# Sentiment Analysis on Trending Events

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://bitbucket.org/trendanalysis/trendanalyserpy/src/master/)

The proliferation of social media in the recent past has provided end users a powerful platform to voice their opinions.  
 
It becomes critical to understand the polarity of these opinions in order to understand user orientation on diverse set of entities and thereby make a smarter decisions.

In this project we are perfroming sentiment Analysis on the following trends.

  - Twitter HashTag Analysis
  - Indian Premier League Sentiment Analysis
  - Project 3 title
  - Project 4 title

##### Twitter HashTag Analysis
  - Trends on social media keeps us know what�s happening around the world. In   this analysis we use sentiment analysis to predict the nature of Trends and   demographic opinion on the trends.
  
###### Packages used
Twitter Hashtag Anlysis uses a number of open source projects to work properly:

* TextBlob v3.5.0 - TextBlob is a Python (2 and 3) library for processing textual data. 
* Pyrebase v3.0.27 - A simple python wrapper for the Firebase API.
* Pymongo v3.8.0 - PyMongo is a Python distribution containing tools for working with MongoDB,
* DateTime v4.3 - The datetime module supplies classes for manipulating dates and times in both simple and complex ways. While date and time arithmetic is supported, the focus of the implementation is on efficient attribute extraction for output formatting and manipulation. For related functionality, see also the time and calendar modules.

##### Installation

The Twitter Sentiment Analysis can be executed by running the Sentiment.py file.

Install the dependencies and devDependencies and start the Senimtent.py.

```sh
$ pip install -U Textblob
$ pip install Pyrebase
$ pip install pymongo
$ pip install pip install DateTime
$ pip install -U Tweepy
```
##### Steps to Run
Select the Sentiment.py file and run in any Python 3 supported environment,IDEs such as Eclipse-PyDev, PyCharm, Eric, and NetBeans also allow to run Python scripts from inside the environment.

###### Running Modules With the -m Option
$ python3 -m Sentiment.py
###### Using the Script Filename
C:\devspace> Sentiment.py

#### Indian Premier League Sentiment Analysis
    - Streaming data analysis in real time has become so fast and efficient for obtaining useful
      information about what is happening now. We are focusing on building a data processing
      system that creates informative data regarding IPL (Indian Premier League) matches and
      correlates the sentiment of fans to match play.

###### Packages used
Indian Premier League Sentiment Analysis uses a number of open source projects to work properly:

* TextBlob v3.5.0 - TextBlob is a Python (2 and 3) library for processing textual data. 
* Pyrebase v3.0.27 - A simple python wrapper for the Firebase API.
* Pymongo v3.8.0 - PyMongo is a Python distribution containing tools for working with MongoDB,
* DateTime v4.3 - The datetime module supplies classes for manipulating dates and times in both simple and complex ways. While date and time arithmetic is supported, the focus of the implementation is on efficient attribute extraction for output formatting and manipulation. For related functionality, see also the time and calendar modules.

##### Installation

The Twitter Sentiment Analysis can be executed by running the Sentiment.py file.

Install the dependencies and devDependencies and start the Senimtent.py.

```sh
$ pip install -U Textblob
$ pip install Pyrebase
$ pip install pymongo
$ pip install pip install DateTime
$ pip install -U Tweepy
```
##### Steps to Run
Select the Sentiment.py file and run in any Python 3 supported environment,IDEs such as Eclipse-PyDev, PyCharm, Eric, and NetBeans also allow to run Python scripts from inside the environment.

###### Running Modules With the -m Option
$ python3 -m Sentiment.py
###### Using the Script Filename
C:\devspace> Sentiment.py












