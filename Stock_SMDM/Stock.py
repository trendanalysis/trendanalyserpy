#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 11 23:23:34 2019

@author: vijay
"""
from pandas_datareader import data as pdr
import matplotlib.pyplot as plt
import fix_yahoo_finance as yf
yf.pdr_override() 


class StockValue:
    def __init__(self):
        print('StockValue class initialised')
    
        
    def getStockDataFor(self,companyName,startDate,endDate):
        data = pdr.get_data_yahoo(companyName, start=startDate, end=endDate)
        return data
    
    
    
    
def main():
    
    stockData = StockValue()
    stockValue = stockData.getStockDataFor("TEAMJ","2019-04-01","2019-04-10")
    print(stockValue)
    symbol='AMZN'
    stockValue.to_csv("~/Desktop/{}.csv".format(symbol))
    # select only close column
    close = stockValue[['Close']]
    # rename the column with symbole name
    close = close.rename(columns={'close': symbol})
    ax = close.plot(title='Amazon')
    ax.set_xlabel('date')
    ax.set_ylabel('close price')
    ax.grid()
    plt.show()
    
if __name__ == "__main__":
    main()  