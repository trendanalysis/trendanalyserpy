#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 10 19:00:15 2019

@author: vijay
"""
from trendingHashtags import TrendingHashTag
from CollectTweets import CollectTweets
from textblob import TextBlob
from datetime import date
import datetime

import pyrebase


config = {
    "apiKey": "AIzaSyCwoFUiKkac-7CgMGyPox4Pxo1DxeF4oUA",
    "authDomain": "smdm-7e8c5.firebaseapp.com",
    "databaseURL": "https://smdm-7e8c5.firebaseio.com",
    "projectId": "smdm-7e8c5",
    "storageBucket": "smdm-7e8c5.appspot.com",
    "messagingSenderId": "866684119908"
}

email = 'testaccount@firebase.com'
passcode = 'testAcount'

def convert(list): 
    res = str("".join(map(str, list))) 
      
    return res 

class SentimentAnalysis:
        def getPolarity(self,text):
            
            blob = TextBlob(text)
            blob.tags          
            blob.noun_phrases
            polarity = 0.0
            for sentence in blob.sentences:
                polarity += sentence.sentiment.polarity
            return polarity
        
        def findPolarityforHashTag(self,hashTag):
                        
            collect = CollectTweets(hashTag)
            list_tweets = collect.collectTweets()
            positive = 0
            negative = 0
            hashTagPolarity = {
                    "positive":positive,
                    "negative":negative
                    }
            for tweet in list_tweets:
                result = self.getPolarity(convert(tweet))
                if  result < 0.0:
                    val = hashTagPolarity["negative"] 
                    val = val+1
                    hashTagPolarity["negative"]  = val
                    
                elif result > 0.0:
                    val = hashTagPolarity["positive"] 
                    val = val+1
                    hashTagPolarity["positive"]  = val

            return hashTagPolarity
            
        



def main():

    sentiment = SentimentAnalysis()
    fireBase = pyrebase.initialize_app(config)
    db = fireBase.database()
    auth = fireBase.auth()
        
    user = auth.sign_in_with_email_and_password(email,passcode)
    collect = CollectTweets('')
    lists = collect.getTrendingTag()
    data ={}
    resultList = []
    for name in lists:
#        print(name['tweet_hashtag'] ,"->",name['tweet_volume'],"sample 250 tweets Polarity ->",sentiment.findPolarityforHashTag(name['tweet_hashtag']))
        data = {
                "name":name['tweet_hashtag'],
                "tweet_volume":name['tweet_volume'],
                "polarity":sentiment.findPolarityforHashTag(name['tweet_hashtag'])
                }
        resultList.append(data)
    date_ = date.today()- datetime.timedelta(0)  
    result = db.update({str(date.today().strftime('%Y-%m-%d')):resultList})
    tags = TrendingHashTag()
    tags.printList(23424977,'pymongo_test28'+"+"+str(date_.strftime('%Y-%m-%d')))
    


    
    
if __name__ == "__main__":
    main()
