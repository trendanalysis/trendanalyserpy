#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 12 01:28:27 2019

@author: vijay
"""

from pymongo import MongoClient
client = MongoClient('localhost', 27017)
db = client['pymongo_test28+2019-04-14']
column = db[str(23424977)]
cursor = column.find().sort('tweet_volume',-1)

for x in cursor:
    print(x)
